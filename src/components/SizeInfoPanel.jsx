import React from 'react';
import classNames from 'classnames';
import {AppContext} from "./App";

function SizeInfoPanel(props) {
    return (
        <AppContext.Consumer>
            {context =>
                <div className={classNames('infoPanel', context.theme)}>
                    <p>Height: {props.componentSize.height}</p>
                    <p>Width: {props.componentSize.width}</p>
                </div>
            }
        </AppContext.Consumer>
    )
}

export default SizeInfoPanel;