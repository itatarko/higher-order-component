import React from 'react';
import ContentPanel from "./ContentPanel";
import withSizeInfo from "../hoc/withSizeInfo";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export const AppContext = React.createContext();

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: 'darkTheme'
        };
        this.handleThemeChange = this.handleThemeChange.bind(this);
    }

    handleThemeChange = () => {
        this.setState(state => ({
            theme: state.theme === 'darkTheme' ? 'lightTheme' : 'darkTheme'
        }));
    };

    render() {
        const ComponentWithSizeInfo = withSizeInfo(ContentPanel);
        return (
            <div>
                <FormControlLabel
                    control={
                        <Switch checked={this.state.theme === 'darkTheme'}
                                onChange={this.handleThemeChange}
                                color="primary"
                                inputProps={{'aria-label': 'primary checkbox'}}/>
                    }
                    label="Switch theme"
                />
                <AppContext.Provider value={{...this.state}}>
                    <ComponentWithSizeInfo />
                </AppContext.Provider>

            </div>
        );
    }
}

export default App;