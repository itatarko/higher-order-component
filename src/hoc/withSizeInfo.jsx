import React from 'react';

import { AppContext } from '../components/App';
import SizeInfoPanel from "../components/SizeInfoPanel";
import ResizeObserver from "react-resize-observer";
import classNames from "classnames";

export default function withSizeInfo(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                componentSize: {height: 0, width: 0},
            };
        }

        handleResize = (event) => {
            this.setState(() => ({
                componentSize: {
                    height: event.height,
                    width: event.width
                }
            }));
        };

        render() {
            return (
                <AppContext.Consumer>
                    {context =>
                        <div>
                            <SizeInfoPanel componentSize={this.state.componentSize} />
                            <div className={classNames('contentPanel', 'resizeable', context.theme)}>
                                <ResizeObserver onResize={this.handleResize}/>
                                <WrappedComponent />
                            </div>
                        </div>
                    }
                </AppContext.Consumer>

            )
        }
    };
}